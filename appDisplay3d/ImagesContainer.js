import Image from "./Image";
import {Object3D} from "three";
import autoBind from "react-autobind";

class ImagesContainer extends Object3D {
	constructor(imagePath) {
		super();
		this.imagePath = imagePath;
		autoBind(this);
	}
	loadImages(imagesData) {
		const promises = [];
		this.images = imagesData.map(imageData => {
			const image = new Image();
			promises.push(image.load(imageData, this.imagePath));
			return image;
		});
		this.add(...this.images);
		return Promise.all(promises);
	}
}
export default ImagesContainer;
