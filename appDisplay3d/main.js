import axios from "axios";
import ThreeCore from "./ThreeCore";
import ImagesContainer from "./ImagesContainer";
import config from "./config.json";
import {TimelineMax} from "gsap";
const dataUrl = "/apps/flashSQLinterface/read3.php?table=stillstring_images&where=string_id=";
const imagePath = "/utilities/getty_importer/tmp/";


const threeCore = new ThreeCore(window.innerWidth, window.innerHeight);
window.document.body.appendChild(threeCore.getRendererElement());

const timeline = new TimelineMax();

const imagesContainer = new ImagesContainer(imagePath);
threeCore.addChild(imagesContainer);
window.addEventListener("resize", threeCore.onWindowResize, false);

const animate = () => {
	threeCore.render();
	window.requestAnimationFrame(animate);
};

window.loadContent = (id) => {
	return new Promise((resolve, reject) => {
		axios.get(dataUrl + id)
			.then(response => {
				const dataUnlooped = response.data.map(image => Object.assign({}, image, {order:parseInt(image.order)}));
				const data = createLoop(dataUnlooped, config.defaultDuration, config.minTotalDuration);
				imagesContainer.loadImages(data)
					.then(tweenGroups => {
						initTimeline(timeline, tweenGroups);
						threeCore.render();
						resolve();
					})
					.catch(err => reject(err));
			}).catch(err => reject(err));
	});
};

const initTimeline = (timeline, tweenGroups) => {
	const tweens = tweenGroups.reduce((acc, tweenGroup) => acc.concat(tweenGroup), []);
	timeline.add(tweens);
	timeline.pause();
	setTimeout(() => {
		timeline.progress(0);
	}, 4000);
};

const createLoop = (dataOrig, duration, minTotalDuration) => {
	const data = [].concat(dataOrig);
	let index = 0;
	let order = data.length;
	const originalLength = data.length;
	while (data.length * duration < minTotalDuration) {
		const clone = Object.assign({}, data[index], {order:order});
		data.push(clone);
		order++;
		index++;
		if (index >= originalLength) {
			index = 0;
		}
	}
	return data;
};

window.startAnimation = animate;
window.timelinePlay = () => timeline.play();
window.timelinePause = () => timeline.pause();
window.updateTweenTime = time => {
	if (time < 0.05) {
		time = 0.05;
	}
	return new Promise(resolve => {
		timeline.time(time);
		threeCore.render();
		resolve();
	});
};
window.updateTweenProgress = pct => {
	if (pct < 0.005) {
		pct = 0.005;
	}
	return new Promise(resolve => {
		timeline.progress(pct);
		threeCore.render();
		resolve();
	});
};
