import {Object3D, PlaneGeometry, MeshBasicMaterial, Mesh, TextureLoader} from "three";
import config from "./config.json";
import TweenMax from "gsap";
import autoBind from "react-autobind";

class Image extends Object3D {
	constructor() {
		super();
		autoBind(this);
	}
	load(data, imagePath) {
		return new Promise((resolve, reject) => {
			Image.textureLoader.load(imagePath + data.filename,
				tex => {
					const imageMesh = this.createImage(tex, data);
					this.add(imageMesh);
					this.position.set(...this.getInitialPosition(imageMesh, data, config));
					const tweenVals = this.calculateTweenVals(data, imageMesh.geometry.parameters, config);
					resolve(this.createTweens(tweenVals, data.order, this.scale, this.position, imageMesh.material));
				},
				progress => {
				},
				err => {
					reject(err);
				}
			);
		});
	}
	getInitialPosition(mesh, data, config) {
		const geoParams = mesh.geometry.parameters;
		const position = this.calculatePosition(data.roiX, data.roiY, geoParams.width, geoParams.height, 1, config.displaySize);
		return[position.x, position.y, data.order];
	}
	createImage(tex, data) {
		const mesh = this.createMesh(tex);
		mesh.material.opacity = data.order === 0 ? 1 : 0;
		return mesh;
	}
	createTweens(tweenVals, order, scale, position, material) {
		return [
			new TweenMax.to(scale, tweenVals.duration, {x:config.maxScale, y:config.maxScale, delay:tweenVals.inDelay, ease:Linear.easeInOut}),
			new TweenMax.to(position, tweenVals.duration, {x:tweenVals.x, y:tweenVals.y, delay:tweenVals.inDelay, ease:Linear.easeInOut}),
			new TweenMax.to(material, tweenVals.transDuration, {opacity:1, delay:tweenVals.inDelay})
		];
	}
	calculatePosition(roiX, roiY, width, height, scale, displaySize) {
		const extraPixelsH = width * scale - displaySize.width;
		const extraPixelsV = height * scale - displaySize.height;
		const translatePctH = roiX * 2 - 1;
		const x = extraPixelsH / 2 * -translatePctH;
		const translatePctV = roiY * 2 - 1;
		const y = extraPixelsV / 2 * translatePctV;
		return({x,y});
	}
	calculateTweenVals(data, geoParams, config) {
		const translate = this.calculatePosition(data.roiX, data.roiY, geoParams.width, geoParams.height, config.maxScale, config.displaySize);
		const inDelay = data.order === 0 ? 0 : data.order * config.defaultDuration - config.transitionDuration;
		return {
			inDelay,
			x:translate.x,
			y:translate.y,
			duration:config.defaultDuration + config.transitionDuration,
			scale:config.maxScale,
			transDuration:config.transitionDuration
		};
	}
	createMesh(tex) {
		const size = this.calculateInitialSize(tex.image.width, tex.image.height);
		const geom = new PlaneGeometry(size.width,size.height);
		const material = new MeshBasicMaterial({map:tex, transparent:true});
		return new Mesh(geom, material);
	}
	calculateInitialSize(width, height) {
		const aspect = width/height;
		if (aspect <= config.displaySize.width / config.displaySize.height) {
			return {width:config.displaySize.width, height:config.displaySize.width/aspect};
		} else {
			return {width:config.displaySize.height*aspect, height:config.displaySize.height};
		}
	}
	static textureLoader = new TextureLoader();
}
export default Image;
