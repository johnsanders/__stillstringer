import {Scene, OrthographicCamera, WebGLRenderer} from "three";
import autoBind from "react-autobind";

class ThreeCore {
	constructor(width, height) {
		this.scene = new Scene();
		this.camera = new OrthographicCamera(-1920/2, 1920/2, 1080/2, -1080/2);
		this.camera.position.z = 100;
		this.renderer = new WebGLRenderer({antialias:true});
		this.renderer.setSize(width, height);
		this.onWindowResize = this.onWindowResize.bind(this);
		autoBind(this);
	}
	onWindowResize(e) {
		this.renderer.setSize(e.target.innerWidth, e.target.innerHeight);
	}
	addChild(child) {
		this.scene.add(child);
	}
	getRendererElement() {
		return this.renderer.domElement;
	}
	setCameraPosition(x,y,z) {
		this.camera.position.set(x,y,z);
	}
	render() {
		this.renderer.render(this.scene, this.camera);
	}
}

export default ThreeCore;
