<?php
	if (!isSet($_POST["name"]) || !isSet($_POST["images"]) || !isSet($_POST["msNumber"]) || !isSet($_POST["email"])) {
		exitWithError("Need name, email, msNumber and images");
	}
	$name = $_POST["name"];
	$email = $_POST["email"];
	$msNumber = $_POST["msNumber"];
	$images = json_decode($_POST["images"]);

	$dbLink = mysqli_connect('localhost', 'root', 'root', "cnniwall");
	if (!$dbLink) {
		exitWithError(mysql_error());
	}
	$query = "INSERT INTO `cnniwall`.`status_render`
		 (`id`, `type`, `data1`, `name`, `email`, `msNumber`, `started`, `finished`, `status`)
		 VALUES
		 (NULL, 'stillstring', NULL, '$name', '$email', '$msNumber', NULL, NULL, 'Queued');";
	$result = mysqli_query( $dbLink, $query );

	if ($result) {
		$stringId = mysqli_insert_id($dbLink);
	} else {
		exitWithError( mysqli_error($dbLink) );
	}

	for ($i=0; $i<count($images); $i++) {
		$img = $images[$i];
		$query = "INSERT INTO `cnniwall`.`stillstring_images`
			 (`id`, `string_id`, `source`, `imageId`, `roiX`, `roiY`, `order`)
			 VALUES
			 (NULL, $stringId, 'getty', $img->id, $img->roiX, $img->roiY, $img->order);";
		$result = mysqli_query($dbLink, $query);
		if (!$result) {
			exitWithError( mysqli_error($dbLink) );
		}
	}
	json_encode(["status" => "ok"]);

	file_put_contents('/home/cnnitouch/touch.log', 'Stillstring to MS ' . $name . ' ' . $email . "\n", FILE_APPEND);
	function exitWithError($msg) {
		echo json_encode( ["status" => "error", "message" => $msg] );
		exit;
	}

?>
