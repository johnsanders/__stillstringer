<?php

//sleep(1);
//echo '12345';
//exit;

	require("getAuthToken.php");
	require("msCreds.php");

	if ( !isSet($_POST['summary']) || !isSet($_POST['slug']) || !isSet($_POST['source']) || !isSet($_POST['location'])) {
		exitWithError();
	}

	$locationHosts = [ "atlanta" => "msatlanta.turner.com",
				   "london" => "mslondon.turner.com"];
	$source = $_POST['source'];
	$slug = $_POST['slug'];
	$summary = $_POST['summary'];
	$locationHost = $locationHosts[ $_POST['location'] ];
	$endpoint = "http://$locationHost/omneonserver/rest/cnn.bmam?";

	$createProps = ['speaker', 'country', 'city', 'county', 'otherLocation', 'reporter', 'photoJournalist', 'producer', 'cnnAssetId',
					  'slug', 'shotDate', 'shotDateEntered', 'category', 'categoryName', 'type', 'typeName', 'summary', 'videoSource',
				  	  'videoRestrictions', 'alerts', 'embargo', 'state', 'editor', 'cutFrom', 'nativeAspectRatio', 'technicalNotes', 'assetElements'];
	$requiredCreateProps = ['slug', 'typeName', 'type', 'category', 'videoSource'];
	$editProps = ['editor', 'editInstructions', 'cutFrom', 'editBay', 'outCue', 'editStatusDisplay', 'editStatus', 'productionStartDate', 'productionEndDate', 'editorComments', 'division',
				  'cnnField', 'cnniField', 'hdlnField', 'otherField', 'program', 'assignmentTypes', 'totalRunTime', 'cnnAssetId', 'inNewsdesk', 'returnFormat', 'method', 'authenticationToken'];
    $requiredEditProps = ['cnniField', 'program'];

	$authToken = getAuthToken($endpoint, $user , $pass );
	if (!$authToken){
		exitWithError();
	} else {
		sleep(1);
	}

	$assetId = createPrecut($endpoint, $user, $pass, $source, $summary, $slug, $authToken, $createProps, $requiredCreateProps, $locationHost);

	if ($assetId == "0"){
		exitWithError();
	} else {
		sleep(2);
		if ( lockUnlock('lockAsset', $assetId, $authToken, $endpoint) ) {
			sleep(2);
			$result = addEditData($endpoint, $assetId, $authToken, $editProps, $requiredEditProps, $locationHost);
			verboseLog($result);
			if ($result == "0"){
				exitWithError();
			} else {
				sleep(2);
				if (lockUnlock('unlockAsset', $assetId, $authToken, $endpoint)) {
					echo $assetId;
				} else {
					exitWithError();
				}
			}
		} else {
			exitWithError();
		}
	}
	exit;

	function createPrecut($endpoint, $user, $pass, $source, $summary, $slug, $authToken, $props, $requiredProps, $locationHost){
		$options = [
			"slug" => $slug,
			"category" => '10002',
			"type" => '10041',
			"typeName" => 'VO/STILL',
			"nativeAspectRatio" => '10002',
			"summary" => $summary, 
			"videoSource" => $source,
			"cnniField" => '29286',
			"authenticationToken" => $authToken,
			"category" => '10002',
			"returnFormat" => 'json',
			"method" => 'createAsset'
		];
		$request = buildRequest($options, $props, $requiredProps);
		$result = sendRequest($endpoint, $request, $locationHost);
		verboseLog($result);
		if ($result->response->status == 'OK') {
			$assetId = trim($result->response->payload->cnnAssetId);
			return( $assetId );
		} else {
			return "0";
		}
	}


	function addEditData($endpoint, $assetId, $authToken, $props, $requiredProps, $locationHost) {
		$options = [
			"method" => "saveAssetEdits",
			"cnnAssetId" => $assetId,
			"cnniField" => "29286",
			"program" => "29286",
			"editStatus" => "NOT_STARTED",
			"editStatusDisplay" => "Not Started",
			"inNewsdesk" => "false",
			"returnFormat" => "json",
			"authenticationToken" => $authToken
		];
		$request = buildRequest($options, $props, $requiredProps);
		$result = sendRequest($endpoint, $options, $locationHost);
		verboseLog($result);
		if ($result->response->status == 'OK') {
			return $result;
		} else {
			return '0';
		}
	}


	function lockUnlock( $which, $assetId, $authToken, $endpoint ) {
		$getUrl = $endpoint . "&_dc=||TIMESTAMP||&cnnAssetId=||ASSETID||&lockGroup=EDITS&returnFormat=json&method=||METHOD||&authenticationToken=||AUTHTOKEN||";

		$url = str_replace("||TIMESTAMP||", strval(time()) . "000", $getUrl);
		$url = str_replace("||ASSETID||", $assetId, $url);
		$url = str_replace("||METHOD||", $which, $url);
		$url = str_replace("||AUTHTOKEN||", urlencode($authToken), $url);
		$result = file_get_contents($url);
		$result = json_decode($result);
		verboseLog($result);
		return $result->response->status == "OK";
	}

	function buildRequest($data, $props, $requiredProps) {
		foreach ($requiredProps as $prop) {
			if (!isSet($data[$prop]) || $data[$prop] == ''){
				echo $prop;
				return false;
			}
		}
		foreach ($props as $prop){
			if (!isSet($data[$prop])){
				$data[$prop] = "";
			}
		}
		return($data);
	}

	function sendRequest($endpoint, $data, $locationHost){
		$data = http_build_query($data);
		$context_options = array (
	        'http' => array (
	            'method' => 'POST',
	            'header'=>
						   "Host: $locationHost\r\n" .
						   "Connection: keep-alive\r\n" .
						   "Content-Length: " . strlen($data) . "\r\n" .
						   "Origin: http://$locationHost\r\n" .
						   "X-Requested-With: XMLHttpRequest\r\n" .
						   "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36\r\n" .
						   "Content-type: application/x-www-form-urlencoded; charset=UTF-8\r\n" .
	                	   "Accept: */*\r\n" .
						   "Referer: http://$locationHost/bmam/app.html\r\n" .
						   "Accept-Language: en-US,en;q=0.8,es;q=0.6\r\n" .
						   "Cookie: ys-bmam.login.rememberMe.username=s%3A\r\n",
	            'content' => $data
	            )
	        );
		$context = stream_context_create($context_options);
		$result = file_get_contents($endpoint, false, $context);
		$result = json_decode($result);
		return $result;
	}

	function exitWithError(){
		echo "0";
		exit;
	}
	function verboseLog($val){
		if ( isSet($_POST['verbose']) ){
			var_dump($val);
			echo "\n<br>\n<br>\n<br>\n<br>\n<br>";
		}
	}
?>
