
<?php
	function getAuthToken($endpoint, $username, $password){
		$data = [
					"username" => $username,
					"password" => $password,
					"method" => "getAuthToken",
					"returnFormat" => "json",
					"domain" => "BMAM"
				];
		$data = http_build_query($data);
		$context_options = array (
	        'http' => array (
	            'method' => 'POST',
	            'header'=>
						   "Host: msatlanta.turner.com\r\n" .
						   "Connection: keep-alive\r\n" .
						   "Content-Length: " . strlen($data) . "\r\n" .
						   "Origin: http://msatlanta.turner.com\r\n" .
						   "X-Requested-With: XMLHttpRequest\r\n" .
						   "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36\r\n" .
						   "Content-type: application/x-www-form-urlencoded; charset=UTF-8\r\n" .
	                	   "Accept: */*\r\n" .
						   "Referer: http://msatlanta.turner.com/bmam/app.html\r\n" .
						   "Accept-Language: en-US,en;q=0.8,es;q=0.6\r\n" .
						   "Cookie: ys-bmam.login.rememberMe.username=s%3A\r\n",
	            'content' => $data
	            )
	        );
		$context = stream_context_create($context_options);
		$result = file_get_contents($endpoint, false, $context);

		$result = json_decode($result);

		if ($result->response->status == 'OK') {
			return $result->response->payload->authenticationToken;
		} else {
			return false;
		}
	}

?>
