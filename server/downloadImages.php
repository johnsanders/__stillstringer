<?php
	if (!isSet($_GET["id"])){
		exitWithError("Need a stillstring id.");
	}
	$id = $_GET["id"];

	$images = file_get_contents("http://localhost/apps/flashSQLinterface/read3.php?table=stillstring_images&where=string_id=$id");
	$images = json_decode($images);

	$dbLink = mysqli_connect('localhost', 'root', 'root', "cnniwall");
	for ($i=0; $i<count($images); $i++){
		if ($images[$i]->source === "getty") {
			$id = $images[$i]->id;
			$imageId = $images[$i]->imageId;
			$filename = $images[$i]->filename;
			if ($filename != "" && file_exists( "/home/cnnitouch/www/utilities/getty_importer/tmp/$filename" )) {
				continue;
			}
			echo "Downloading image " . strval($i+1);
			$downloadInfo = file_get_contents("http://localhost/utilities/getty_importer/get_image.php?tag=&id=$imageId");
			$downloadInfo = json_decode($downloadInfo);
			if ($downloadInfo->status !== "ok") {
				exitWithError($downloadInfo->message);
			}
			$filename = $downloadInfo->filename;
			$images[$i]->filename = $filename;
			$query = "UPDATE `cnniwall`.`stillstring_images` 
				SET filename='$filename' WHERE id='$id'"; 
			$result = mysqli_query($dbLink, $query);
			if (!$result) {
				exitWithError( mysqli_error($dbLink) );
			}
			sleep(1);
		}
	}

	mysqli_close($dbLink);

	function exitWithError($msg) {
		echo json_encode( ["status" => "error", "message" => $msg] );
		exit;
	}
?>
