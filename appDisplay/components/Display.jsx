import React from "react";
import axios from "axios";
import autobind from "react-autobind";
import DisplayImage from "./DisplayImage";

class Display extends React.Component {
	static size = {w:1920, h:1080};
	static aspect = Display.size.w / Display.size.h;
	static minDuration = 60;
	constructor(props) {
		super(props);
		this.state = {images:[], animPct:0};
		this.components = [];
		autobind(this);
		window.updateTweenTime = this.updateTweenTime;
		window.loadStillstring = this.loadStillstring;
		window.loadContent = this.loadStillstring;
	}
	loadStillstring(id, stillDuration=5) {
		axios.get("/apps/flashSQLinterface/read3.php?table=stillstring_images&where=string_id=" + id)
			.then((res) => {
				let images = res.data;
				let count = 0;
				while (images.length * stillDuration < Display.minDuration) {
					images.push(Object.assign({}, images[count], {order:images.length}));
					count++;
				}
				this.setState({images:res.data, stillDuration:stillDuration});
			})
			.catch((err) => console.error(err));
		return new Promise((resolve, reject) => {
			let count = 0;
			const checkImages = () => {
				if (this.components.reduce((acc, image) => acc && image.state.loaded, true)) {
					resolve("Images loaded for String #" + id);
				} else {
					count++;
					if (count > 10) {
						reject("Failed to load images");
					} else {
						setTimeout(checkImages, 1000);
					}
				}
			};
			setTimeout(checkImages, 1000);
		});
	}
	updateTweenTime(time) {
		return new Promise((resolve, reject) => {
			this.components.forEach(component => component.setTweenTime(time));
			resolve();
		});
	}
	render() {
		return(
			<div
				className="container group"
				style={{
					width:Display.size.w.toString() + "px",
					height:Display.size.h.toString() + "px"
				}}
			>
				{
					this.state.images.map(image => (
						<DisplayImage
							key={"image_" + image.order}
							filename={image.filename}
							order={parseInt(image.order)}
							roiX={parseFloat(image.roiX)}
							roiY={parseFloat(image.roiY)}
							duration={this.state.stillDuration}
							animPct={this.state.animPct}
							screenSize={Display.size}
							displayAspect={Display.aspect}
							ref={ instance => this.components.push(instance) }
						/>
					))
				}
			</div>
		);
	}
}

export default Display;
