import React from "react";
import {TimelineMax} from "gsap";
import autobind from "react-autobind";
import {calculateBasePositionTall, calculateBasePositionWide, calculateDisplaySizeWide, calculateDisplaySizeTall, calculateMaxTranslate} from "../utils/imageHelpers";
import PropTypes from "prop-types";

class DisplayImage extends React.Component {
	static imagePath = "/utilities/getty_importer/tmp/";
	constructor(props) {
		super(props);
		this.state={
			displaySize:{w:100, h:100},
			position:{x:0, y:0},
			scale:1,
			opacity:this.props.order === 0 ? 1 : 0
		};
		autobind(this);
		this.tweens = {scale:1, x:0, y:0, opacity:this.state.opacity};
		this.basePosition = {x:0, y:0};
		this.baseSize = {w:100, y:100};
		this.maxScale = 1.7;
		this.maxTranslate = {x:0, y:0};
		this.loaded = false;
	}
	createTimeline() {
		const transDur = 0.5;
		const fullDur = this.props.duration + transDur * 2;
		const startTime = this.props.order * this.props.duration - transDur;
		this.tl = new TimelineLite({paused:true, onUpdate:this.updateTweenValues});
		this.tl.to(this.tweens, fullDur, {
			scale:this.maxScale,
			x:this.maxTranslate.x,
			y:this.maxTranslate.y,
			ease:Linear.easeInOut}, startTime);
		this.tl.to(this.tweens, transDur, {opacity:1}, startTime);
	}
	setTweenTime(time) {
		this.tl.time(time);
	}
	updateTweenValues() {
		this.setState({
			scale:this.tweens.scale,
			position:{x:this.basePosition.x + this.tweens.x, y:this.basePosition.y + this.tweens.y},
			opacity:this.tweens.opacity
		});
	}
	onImageLoad(e) {
		const w = e.target.naturalWidth;
		const h = e.target.naturalHeight;
		const nativeAspect = w / h;
		if (nativeAspect <= this.props.displayAspect) {
			this.baseSize = calculateDisplaySizeTall(w, h, this.props.screenSize.w);
			this.basePosition.y = calculateBasePositionTall(this.props.screenSize.h, this.baseSize.h, this.props.roiY);
		} else {
			this.baseSize = calculateDisplaySizeWide(w, h, this.props.screenSize.h);
			this.basePosition.x = calculateBasePositionTall(this.props.screenSize.w, this.baseSize.w, this.props.roiX);
		}
		this.maxTranslate = calculateMaxTranslate(this.props.screenSize, this.baseSize, this.basePosition, this.maxScale, this.props.roiX, this.props.roiY);
		this.setState({displaySize:this.baseSize, position:this.basePosition, loaded:true}, this.createTimeline);
	}
	render() {
		return(
			<img
				className="displayImage"
				src={DisplayImage.imagePath + this.props.filename}
				onLoad={this.onImageLoad}
				style={{
					width:(this.state.displaySize.w * this.state.scale).toString() + "px",
					height:(this.state.displaySize.h * this.state.scale).toString() + "px",
					transform:`translate(${this.state.position.x.toString()}px, ${this.state.position.y.toString()}px)`,
					opacity:this.state.opacity,
					zIndex:this.props.order
				}}
			/>
		);
	}
}
DisplayImage.propTypes = {
	order:PropTypes.number.isRequired,
	duration:PropTypes.number.isRequired,
	roiX:PropTypes.number.isRequired,
	roiY:PropTypes.number.isRequired,
	displayAspect:PropTypes.number.isRequired,
	screenSize:PropTypes.object.isRequired,
	filename:PropTypes.string.isRequired
};
export default DisplayImage;
