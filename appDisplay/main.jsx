import React from "react";
import {render} from "react-dom";
import Display from "./components/Display";
import "./styles/style.css";

render(<Display />, document.getElementById("app"));
