export const calculateDisplaySizeTall = (nativeWidth, nativeHeight, displayWidth) => {
	const nativeAspect = nativeWidth / nativeHeight;
	const displayHeight = displayWidth / nativeAspect;
	return {w:displayWidth, h:displayHeight};
};
export const calculateBasePositionTall = (screenHeight, imageHeight, roiY) => {
	const verticalPixelsOutOfFrame = imageHeight - screenHeight;
	const y = verticalPixelsOutOfFrame * roiY * -1;
	return y;
};
export const calculateDisplaySizeWide = (nativeWidth, nativeHeight, displayHeight) => {
	const nativeAspect = nativeWidth / nativeHeight;
	const displayWidth = displayHeight * nativeAspect;
	return {w:displayWidth, h:displayHeight};
};
export const calculateBasePositionWide = (screenWidth, imageWidth, roiX) => {
	const horizPixelsOutOfFrame = imageHeight - screenHeight;
	const x = horizPixelsOutOfFrame * roiX * -1;
	return x;
};
export const calculateMaxTranslate = (screenSize, imageSize, imagePosition, maxScale, roiX, roiY) => {
	const horizPixelsOutOfFrameAtMaxScale = imageSize.w * maxScale - screenSize.w;
	const x = (horizPixelsOutOfFrameAtMaxScale * roiX) * -1 - imagePosition.x;
	const verticalPixelsOutOfFrameAtMaxScale = imageSize.h * maxScale - screenSize.h;
	const y = (verticalPixelsOutOfFrameAtMaxScale * roiY) * -1 - imagePosition.y;
	return {x,y};
};
