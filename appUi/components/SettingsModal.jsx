import React from "react";
import Draggable from "react-draggable";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";

class SettingsModal extends React.Component {
	constructor(props) {
		super(props);
	}
	componentWillUnmount() {
		this.props.onUnmount();
	}
	numToPercent(num) {
		return (num * 100).toString() + "%";
	}
	render() {
		return (
			<div className="settingsModal">
				<div className="background" onClick={this.props.handleDoneClick}/>
				<div className="col-xs-offset-2 col-xs-8">
					<div className="panel panel-primary">
						<div className="panel-heading">
							<div className="panel-title">
								<h3>Image Settings</h3>
							</div>
						</div>
						<div className="panel-body">
							<div
								className="imageDiv"
								style={{minHeight:this.props.imageHeight.toString() + "px"}}
							>
								<Draggable
									bounds="parent"
									onStop={this.props.handleSetRoi}
									onDrag={this.props.handleDrag}
									position={{x:this.props.roiPosX, y:this.props.roiPosY}}
								>
									<div
										className="roi"
										style={{
											display:this.props.imageIsReady ? "" : "none",
											width:this.numToPercent(this.props.roiSize),
											height:this.numToPercent(this.props.roiSize)
										}}
									/>
								</Draggable>
								<img
									src={this.props.activeImage.url}
									onLoad={this.props.handleImageLoad}
									style={{opacity:this.props.imageIsReady ? 1 : 0}}
								/>
							</div>
							<div style={{position:"absolute", left:0, top:0, width:"100%", textAlign:"center", height:"550px", alignItems:"center", justifyContent:"center", display:this.props.imageIsReady ? "none" : "flex"}}>
								<Spinner
									className="spinner"
									fadeIn="none"
									name="double-bounce"
								/>
							</div>
							<h4>
								Only what&apos;s inside the red box will be fully seen. Move it so the photo&apos;s main focus is visible.
							</h4>
							<div className="form-group btn-group">
								<button
									className="btn btn-primary"
									onClick={this.props.handleDoneClick}
								>
									Done
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
SettingsModal.propTypes = {
	handleDoneClick:PropTypes.func.isRequired,
	onUnmount:PropTypes.func.isRequired,
	imageHeight:PropTypes.number.isRequired,
	roiSize:PropTypes.number.isRequired,
	handleSetRoi:PropTypes.func.isRequired,
	handleDrag:PropTypes.func.isRequired,
	imageIsReady:PropTypes.bool.isRequired,
	roiPosX:PropTypes.number,
	roiPosY:PropTypes.number,
	activeImage:PropTypes.shape({
		url:PropTypes.string.isRequired
	}),
	handleImageLoad:PropTypes.func.isRequired
};
export default SettingsModal;
