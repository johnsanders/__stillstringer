import React from "react";
import Nav from "./nav/Nav";
import autobind from "react-autobind";
import SettingsModalContainer from "./SettingsModalContainer";
import Notification from "./Notification";
import StillstringContainer from "./StillstringContainer";
import SubmitButtons from "./SubmitButtons";
import {createPrecut, enqueueRender} from "../helpers/msHelpers";
import validate from "../helpers/validate";

class HomeContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {images:[], modalActive:false, notificationText:"", finished:false,
			notificationStyle:"danger", userEmail:"", waiting:false, stringName:""};
		autobind(this);
	}
	componentDidMount() {
		window.addEventListener("beforeunload", (e) => {
			if (this.state.images.length > 0 && this.state.finished === false) {
				e.returnValue = true;
			}
		});
	}
	addImage(imageInfo) {
		const newImages = this.state.images.concat([imageInfo]);
		this.setState({images:newImages, imageSearch:""}, () => this.openImageSettings(imageInfo));
	}
	clearNotification(delay) {
		setTimeout(() => this.setState({notificationText:""}), delay * 1000);
	}
	setNotification(text, style, duration=5) {
		this.setState({notificationText:text, notificationStyle:style});
		if (duration > 0) {
			this.clearNotification(duration);
		}
	}
	handleSortEnd(info) {
		let newImages = this.state.images.map(image => Object.assign(image, {}));
		const movedImage = newImages.splice(info.oldIndex, 1)[0];
		newImages.splice(info.newIndex, 0, movedImage);
		this.setState({images:newImages});
	}
	handleImageDelete(e) {
		const id = e.target.getAttribute("data-uniqueid");
		const image = this.getImageByUniqueId(parseInt(id));
		const index = this.state.images.indexOf(image);
		const newImages = this.state.images.slice(0,index).concat(this.state.images.slice(index+1));
		this.setState({images:newImages});
	}
	handleOpenImageSettings(e) {
		const id = e.target.getAttribute("data-uniqueid");
		const activeImage = this.getImageByUniqueId(parseInt(id));
		this.openImageSettings(activeImage);
	}
	openImageSettings(activeImage) {
		this.setState({activeImage:activeImage, modalActive:true});
	}
	handleSetRoi(roiX, roiY) {
		const newImage = Object.assign(this.state.activeImage, {roiX, roiY});
		const index = this.state.images.indexOf(newImage);
		const newImages = this.state.images.slice(0);
		newImages[index] = newImage;
		this.setState({images:newImages});
	}
	closeSettingsModal() {
		this.setState({modalActive:false});
	}
	handleReset() {
		this.setState({images:[], notificationText:"", userEmail:"", waiting:false, finished:false, stringName:""});
	}
	handleInputChange(e) {
		const name = e.target.getAttribute("name");
		this.setState({[name]:e.target.value});
	}
	assignImageOrders(images) {
		return images.map((img, index) => Object.assign(img, {order:index}));
	}
	getImageByUniqueId(id) {
		return this.state.images.filter(image => image.uniqueId === id)[0];
	}
	handleSubmit() {
		if (this.state.finished || this.state.waiting) { return; }
		const error = validate(this.state.stringName, this.state.userEmail, this.state.images);
		if (error) {
			//this.setNotification(warning[0], warning[1]);
			return;
		}
		this.setState({waiting:true, images:this.assignImageOrders(this.state.images)}, () => {
			createPrecut(this.state.stringName, this.state.images)
				.then(msNumber => {
					enqueueRender(this.state.stringName, this.state.userEmail, msNumber, this.state.images)
						.then(() => {
							this.setState({waiting:false, finished:true});
							this.setNotification("Your MS number will be <b>" + msNumber + "</b>.  We'll email you when it's ready", "success", -1);
						}).catch(err => console.error(err));
				}).catch(err => console.error(err));
		});
	}
	render() {
		return (
			<div>
				<Nav />
				<SettingsModalContainer
					active={this.state.modalActive}
					activeImage={this.state.activeImage}
					handleSetRoi={this.handleSetRoi}
					handleDoneClick={this.closeSettingsModal}
				/>
				<StillstringContainer
					style={{marginTop:"60px"}}
					stringName={this.state.stringName}
					images={this.state.images}
					addImage={this.addImage}
					handleSortEnd={this.handleSortEnd}
					handleImageDelete={this.handleImageDelete}
					handleOpenImageSettings={this.handleOpenImageSettings}
					modalActive={this.state.modalActive}
					handleInputChange={this.handleInputChange}
					setNotification={this.setNotification}
				/>
				<SubmitButtons
					handleSubmit={this.handleSubmit}
					userEmail={this.state.userEmail}
					handleInputChange={this.handleInputChange}
					waiting={this.state.waiting}
					finished={this.state.finished}
					handleReset={this.handleReset}
				/>
				<Notification
					text={this.state.notificationText}
					style={this.state.notificationStyle}
				/>
			</div>
		);
	}
}
export default HomeContainer;
