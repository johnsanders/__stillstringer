import React from "react";
import Timeline from "./Timeline";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";

const Stillstring = (props) => (
	<div className={props.modalActive ? "blur" : ""}>
		<div className="page-header" style={{marginTop:"80px"}}>
			<h1 className="text-center text-uppercase">STILLSTRINGER</h1>
			<h4 className="text-center">Add up to 10 Getty Image ids and we&rsquo;ll render an animated still string to MediaSource</h4>
		</div>

		<div className="form-group group">
			<div className="row">
				<div className="col-xs-12 text-center">
					<label htmlFor="stringName">
						Slug for MediaSource
					</label>
				</div>
			</div>
			<div className="row">
				<div className="col-xs-12 text-center">
					<input
						type="text"
						name="stringName"
						className="form-control inlineInput"
						value={props.stringName}
						onChange={props.handleNameInputChange}
					/>
				</div>
			</div>
		</div>
		<div className="form-group group">
			<div className="col-xs-12 text-center" style={{marginTop:"30px"}}>
				<input
					style={{marginLeft:"30px"}}
					type="text"
					name="imageSearch"
					className="form-control inlineInput"
					placeholder="Getty ID"
					value={props.imageSearch}
					onChange={props.handleInputChange}
					onKeyDown={props.handleSearchKeyDown}
				/>
				<button className="btn btn-primary" onClick={props.handleAddImageClick}>
					Add
				</button>
				<Spinner
					className="spinner"
					fadeIn="none"
					name="double-bounce"
					style={{
						opacity:props.waiting ? "1" : "0",
						display:"inline-block",
						position:"relative",
						left:"10px",
						top:"8px"
					}}
				/>
			</div>
		</div>
		<div className="form-group">
			<Timeline
				axis="xy"
				images={props.images}
				onSortEnd={props.handleSortEnd}
				lockToContainerEdges={true}
				handleImageDelete={props.handleImageDelete}
				handleOpenImageSettings={props.handleOpenImageSettings}
				useDragHandle={true}
			/>
		</div>
	</div>
);

Stillstring.propTypes = {
	imageSearch:PropTypes.string.isRequired,
	handleInputChange:PropTypes.func.isRequired,
	handleNameInputChange:PropTypes.func.isRequired,
	handleAddImageClick:PropTypes.func.isRequired,
	waiting:PropTypes.bool.isRequired,
	handleImageDelete:PropTypes.func.isRequired,
	handleOpenImageSettings:PropTypes.func.isRequired,
	images:PropTypes.array.isRequired,
	modalActive:PropTypes.bool.isRequired,
	handleSortEnd:PropTypes.func.isRequired,
	stringName:PropTypes.string.isRequired,
	handleSearchKeyDown:PropTypes.func.isRequired
};
export default Stillstring;
