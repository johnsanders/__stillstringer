import React from "react";
import PropTypes from "prop-types";

const LiNavLink = (props) => {
	const {to, exact, strict, activeClassName, className, isActive: getIsActive, ...rest} = props;
	return (
		<li
			className={className}
		>
			<a
				href={to}
				{...rest}
			/>
		</li>
	);
};
LiNavLink.propTypes = {
	to:PropTypes.string,
	exact:PropTypes.bool,
	strict:PropTypes.bool,
	activeClassName:PropTypes.string,
	className:PropTypes.string,
	isActive:PropTypes.bool,
	getIsActive:PropTypes.func
};
export default LiNavLink;
