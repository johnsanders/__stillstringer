import React from "react";
import {SortableElement, SortableHandle} from "react-sortable-hoc";

const imageHeight = 100;
const ImageHandle = SortableHandle(({url, size}) => {
	const aspect = size.width/size.height;
	const imageWidth = imageHeight * aspect;
	return <img src={url} width={imageWidth} height={imageHeight}/>;
});

const TimelineItem = SortableElement(({imageUrl, imageId, imageSize, handleImageDelete, handleOpenImageSettings, uniqueId}) => (
	<div className="timelineItem">
		<ImageHandle url={imageUrl} size={imageSize} />
		<div className="timelineItemLabel">
			Getty {imageId}
		</div>
		<span
			data-uniqueid={uniqueId}
			className="timelineItemButton timelineItemDelete glyphicon glyphicon-remove"
			onClick={handleImageDelete}
		/>
		<span
			data-uniqueid={uniqueId}
			className="timelineItemButton timelineItemSettings glyphicon glyphicon-cog"
			onClick={handleOpenImageSettings}
		/>
	</div>
));

export default TimelineItem;
