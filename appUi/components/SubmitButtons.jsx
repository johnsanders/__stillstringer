import React from "react";
import PropTypes from "prop-types";
import Spinner from "react-spinkit";

const SubmitButtons = (props) => (
	<div>
		<div className="form-group group">
			<div className="col-xs-12 text-center">
				<input
					type="text"
					name="userEmail"
					className="form-control inlineInput"
					style={{marginLeft:"33px"}}
					value={props.userEmail}
					onChange={props.handleInputChange}
					placeholder="Your Turner email"
				/>
				<button
					className={"btn btn-primary" + (props.finished || props.waiting? " disabled" : "")}
					onClick={props.handleSubmit}
				>
					Render Stillstring
				</button>
				<Spinner
					className="spinner"
					fadeIn="none"
					name="double-bounce"
					style={{
						opacity:props.waiting ? 1 : 0,
						display:"inline-block",
						top:"7px",
						left:"10px"
					}}
				/>
			</div>
		</div>
		{
			props.finished ?
				<div className="form-group group">
					<div className="col-xs-offset-5 col-xs-2 text-center">
						<button
							className="btn btn-default"
							onClick={props.handleReset}
						>
							Create Another
						</button>
					</div>
				</div>
				: null
		}
	</div>
);

SubmitButtons.propTypes = {
	userEmail:PropTypes.string.isRequired,
	handleSubmit:PropTypes.func.isRequired,
	handleInputChange:PropTypes.func.isRequired,
	waiting:PropTypes.bool.isRequired,
	finished:PropTypes.bool.isRequired,
	handleReset:PropTypes.func.isRequired
};
export default SubmitButtons;
