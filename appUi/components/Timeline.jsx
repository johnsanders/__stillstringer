import React from "react";
import TimelineItem from "./TimelineItem";
import {SortableContainer} from "react-sortable-hoc";

const Timeline = SortableContainer(({images, handleImageDelete, handleOpenImageSettings}) => {
	return (
		<div className="col-xs-offset-2 col-xs-8 well timeline">
			{
				images.map((image, index) => (
					<TimelineItem
						key={index.toString() + "_" + image.id}
						index={index}
						uniqueId={image.uniqueId}
						imageSize={image.size}
						imageUrl={image.url}
						imageId={image.id}
						handleImageDelete={handleImageDelete}
						handleOpenImageSettings={handleOpenImageSettings}
					/>
				))
			}
		</div>
	);
});
export default Timeline;
