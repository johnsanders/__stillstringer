import React from "react";
import autobind from "react-autobind";
import SettingsModal from "./SettingsModal";
import PropTypes from "prop-types";
import TransitionGroup from "react-transition-group/TransitionGroup";
import {FadeTransition} from "../helpers/Transitions";

class SettingsModalContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {imageIsReady:false, imageHeight:600};
		this.roiSize = 0.85;
		autobind(this);
	}
	onUnmount() {
		this.setState({imageIsReady:false, imageHeight:400});
	}
	handleImageLoad(e) {
		const img = this.props.activeImage;
		const roiPos = this.calculateRoiPosFromPct(img.roiX, img.roiY, e.target.clientWidth, e.target.clientHeight, this.roiSize);
		this.setState({
			roiPosX:roiPos.x,
			roiPosY:roiPos.y,
			imageHeight:e.target.clientHeight,
			imageIsReady:true
		});
	}
	handleDrag(e, obj) {
		this.setState({roiPosX:obj.x, roiPosY:obj.y});
	}
	handleSetRoi(e, obj) {
		const nodeWidth = obj.node.clientWidth;
		const nodeHeight = obj.node.clientHeight;
		const parentWidth = obj.node.parentNode.clientWidth;
		const parentHeight = obj.node.parentNode.clientHeight;
		const roiPercent = this.calculateRoiPctFromPos(obj.x, obj.y, nodeWidth, nodeHeight, parentWidth, parentHeight);
		this.props.handleSetRoi(roiPercent.x, roiPercent.y);
	}
	calculateRoiPosFromPct(pctX, pctY, imageWidth, imageHeight, roiSize) {
		const maxX = imageWidth - imageWidth * roiSize;
		const maxY = imageHeight - imageHeight * roiSize;
		const posX = pctX * maxX;
		const posY = pctY * maxY;
		return {x:posX, y:posY};
	}
	calculateRoiPctFromPos(x, y, nodeWidth, nodeHeight, parentWidth, parentHeight) {
		const maxX = parentWidth - nodeWidth;
		const maxY = parentHeight - nodeHeight;
		const roiX = x / maxX;
		const roiY = y / maxY;
		return {x:roiX, y:roiY};
	}
	render() {
		return(
			<TransitionGroup>
				{
					this.props.active ?
						<FadeTransition>
							<SettingsModal
								activeImage={this.props.activeImage}
								handleDoneClick={this.props.handleDoneClick}
								handleSetRoi={this.handleSetRoi}
								handleDrag={this.handleDrag}
								roiSize={this.roiSize}
								imageHeight={this.state.imageHeight}
								roiPosX={this.state.roiPosX}
								roiPosY={this.state.roiPosY}
								imageIsReady={this.state.imageIsReady}
								handleImageLoad={this.handleImageLoad}
								onUnmount={this.onUnmount}
							/>
						</FadeTransition>
						: null
				}
			</TransitionGroup>
		);
	}
}
SettingsModalContainer.propTypes = {
	activeImage:PropTypes.shape({
		roiX:PropTypes.number.isRequired,
		roiY:PropTypes.number.isRequired
	}),
	active:PropTypes.bool.isRequired,
	handleDoneClick:PropTypes.func.isRequired,
	handleSetRoi:PropTypes.func.isRequired,
	roiPosX:PropTypes.number,
	roiPosY:PropTypes.number
};
export default SettingsModalContainer;
