import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import Stillstring from "./Stillstring";
import PropTypes from "prop-types";

class StillstringContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {imageSearch:"", waiting:false};
		this.imageCount = props.images.length;
		autobind(this);
	}
	handleInputChange(e) {
		const name = e.target.getAttribute("name");
		this.setState({[name]:e.target.value});
	}
	handleAddImageClick(e) {
		if (e) {e.preventDefault();}
		if (this.props.images.length >= 10) {
			this.props.setNotification("You've reached the maximum 10 images.", "danger", 5);
		}
		this.setState({waiting:true});
		this.getInfo(this.state.imageSearch).then((response) => {
			this.setState({waiting:false, imageSearch:""});
			if (this.validateResponse(response) === "ok") {
				const image = response.data.display_sizes[0];
				const imageInfo = {
					id: response.data.id,
					uniqueId: Date.now(),
					url: image.uri,
					title: response.data.title,
					size: {width:image.width, height:image.height},
					roiX: 0.5,
					roiY: 0.0
				};
				this.imageCount++;
				this.props.addImage(imageInfo);
			}
		});
	}
	handleSearchKeyDown(e) {
		if (e.keyCode === 13) { this.handleAddImageClick(); }
	}
	validateResponse(response) {
		let ret = "ok";
		if (response.data.status === "error" || response.data.display_sizes.length < 1) {
			this.props.setNotification(response.data.message, "danger");
			ret = false;
		} else if (response.data.display_sizes[0].width < response.data.display_sizes[0].height) {
			this.props.setNotification("I can't do vertical images just yet. Sorry!", "danger");
			ret = false;
		}
		return ret;
	}
	getInfo(id) {
		return axios.get("/utilities/getty_importer/get_info.php?id=" + id)
			.then(response => response);
	}
	render() {
		return(
			<Stillstring
				modalActive={this.props.modalActive}
				stringName={this.props.stringName}
				imageSearch={this.state.imageSearch}
				handleInputChange={this.handleInputChange}
				handleNameInputChange={this.props.handleInputChange}
				handleAddImageClick={this.handleAddImageClick}
				waiting={this.state.waiting}
				images={this.props.images}
				handleSortEnd={this.props.handleSortEnd}
				handleImageDelete={this.props.handleImageDelete}
				handleOpenImageSettings={this.props.handleOpenImageSettings}
				handleSearchKeyDown={this.handleSearchKeyDown}
			/>
		);
	}
}

StillstringContainer.propTypes = {
	modalActive:PropTypes.bool.isRequired,
	images:PropTypes.array.isRequired,
	handleSortEnd:PropTypes.func.isRequired,
	handleImageDelete:PropTypes.func.isRequired,
	handleOpenImageSettings:PropTypes.func.isRequired,
	addImage:PropTypes.func.isRequired,
	setNotification:PropTypes.func.isRequired,
	handleInputChange:PropTypes.func.isRequired,
	stringName:PropTypes.string.isRequired
};
export default StillstringContainer;
