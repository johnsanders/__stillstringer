import React from "react";
import PropTypes from "prop-types";

const Notification = (props) => (
	props.text.length ?
		<div className="form-group">
			<div
				className={`col-xs-offset-4 col-xs-4 alert alert-${props.style} text-center`}
				dangerouslySetInnerHTML={{__html:props.text}}
			/>
		</div>
		: null
);
Notification.propTypes = {
	text:PropTypes.string.isRequired,
	style:PropTypes.string.isRequired
};

export default Notification;
