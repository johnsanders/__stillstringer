import axios from "axios";

export const createPrecut = (slug, images) => {
	return new Promise((resolve, reject) => {
		let params = new URLSearchParams();
		params.append("slug", slug);
		params.append("location", "london");
		params.append("summary", "Getty Images " + images.reduce((acc, img, index) => acc + (index === 0 ? "" : ", ") + img.id, ""));
		params.append("source", "Getty Images");
		axios.post("/apps/stillstringer/server/createPrecut.php", params)
			.then(precutResponse => resolve(precutResponse.data))
			.catch(err => reject(err));
	});
};

export const enqueueRender = (stringName, userEmail, msNumber, images) => {
	return new Promise((resolve, reject) => {
		let params = new URLSearchParams();
		params.append("name", stringName);
		params.append("email", userEmail);
		params.append("msNumber", msNumber);
		params.append("images", JSON.stringify(images));
		axios.post("/apps/stillstringer/server/enqueueString.php", params)
			.then(enqueueResponse => resolve(enqueueResponse))
			.catch(err => reject(err));
	});
};
