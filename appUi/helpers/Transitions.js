import React from "react";
import CSSTransition from "react-transition-group/CSSTransition";
import PropTypes from "prop-types";
export const ScaleTransition = ({children, ...props}) => (
	<CSSTransition
		{...props}
		timeout={300}
		classNames="scaleTrans"
	>
		{children}
	</CSSTransition>
);
ScaleTransition.propTypes = {
	children:PropTypes.object
};

export const FadeTransition = ({children, ...props}) => (
	<CSSTransition
		{...props}
		timeout={500}
		classNames="fadeTrans"
	>
		{children}
	</CSSTransition>
);

FadeTransition.propTypes = {
	children:PropTypes.object
};
