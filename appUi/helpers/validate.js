export default (name, email, images) => {
	if (name.length < 5) {
		return ["Need a more descriptive slug for Mediasource, please", "danger"];
	}
	if (email.indexOf("@turner.com") === -1 && email.indexOf("@cnn.com") === -1) {
		return ["Need your Turner email please, so we can let you know when it's ready", "danger"];
	}
	if (images.length < 2) {
		return ["Need at least two images please.", "danger"];
	}
	return false;
};
