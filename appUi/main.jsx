import React from "react";
import {render} from "react-dom";
import HomeContainer from "./components/HomeContainer";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import "./styles/style.css";
import "./styles/transitions.css";

render(<HomeContainer />, document.getElementById("app"));
